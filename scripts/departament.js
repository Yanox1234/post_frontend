const getCoords = async () => {
    return await fetch('http://140.82.58.91:3333/api/v1/postAddress/findAll', {
            method: 'GET',
            headers: {
                "Content-Type": "text/plain"
            }
        }
    )
        .then(response => response.json())
        .then(data => data);
}

(async function(){
    const coords = await getCoords();
    ymaps.ready(function () {
    
        console.log(coords);
        var myMap = new ymaps.Map('map', {
                    center: [47.991572,37.798341],
                    zoom: 15
                }, {
                searchControlProvider: 'yandex#search'
            }),
    
            // Создаём макет содержимого.
            MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            );
            
            for (i = 0; i < coords.length; ++i) {

            myPlacemarkWithContent = new ymaps.Placemark([coords[i].xcoordinate ,coords[i].ycoordinate], {
                hintContent: coords[i].address.country + ", " +
                coords[i].address.city + ", " +
                coords[i].address.line1 + ", " +
                coords[i].address.zipcode,
                balloonContent: coords[i].name
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Размеры метки.
                iconImageSize: [48, 48],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-24, -24],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
            
                myMap.geoObjects.add(myPlacemarkWithContent);
            
            }
    });
})()
