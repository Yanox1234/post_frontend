const getCalc = async () => {
    return await fetch('http://140.82.58.91:3333/api/v1/calculator/findGroupedByType', {
        method: 'GET',
        headers: {
            "Content-Type": "text/plain"
        }
    })
      .then(response => response.json())
      .then(data => data);
}

(async function(){
  const calc = await getCalc();
  
  var myParent = document.getElementById("js-input")

  for (let i = 0; i < calc.length; i++) {

    var newLabel = document.createElement("label");
    myParent.appendChild(newLabel);
    newLabel.innerHTML = calc[i].type.name;
    newLabel.classList.add("calc_form-inputs__label")

    var selectInput = document.createElement("select");
    myParent.appendChild(selectInput).id = `typeName${'' + i}`;

    for (let j = 0; j < calc[i].calculatorCategory.length; j++) {
      var optinInpit = document.createElement('option');
      document.getElementById(`typeName${'' + i}`).appendChild(optinInpit);
      optinInpit.id = `typeName${'' + i + j}`;
      optinInpit.innerHTML = `<p>${calc[i].calculatorCategory[j].name}</p> - <p>${calc[i].calculatorCategory[j].price}	&#8381;</p>`;
      optinInpit.value = +calc[i].calculatorCategory[j].price;
    }

  }

  var calcBtn = document.createElement('button');
  myParent.appendChild(calcBtn);
  calcBtn.innerHTML = 'Рассчитать стоимость';
  calcBtn.type="submot"
  calcBtn.classList.add("calc_form-inputs__btn")
  calcBtn.classList.add("track_content__form-btn");
})()

const form = document.querySelector('form');
form.addEventListener('submit', calculateProgram);

function calculateProgram(event) {
    event.preventDefault();

    const width = regexForInput(document.querySelector('input[id = "width"]').value);//ширина
    const long = regexForInput(document.querySelector('input[id = "long"]').value);//длина
    const height = regexForInput(document.querySelector('input[id = "height"]').value);//высота
    const weight = regexForInput(document.querySelector('input[id = "weight"]').value);//вес

    if (isNaN(width) || isNaN(long) || isNaN(height) || isNaN(weight)) {
        const isNanValue = isNaN(width) ? "Параметр ширины является отрицательным или 0" :
            isNaN(long) ? "Параметр длины является отрицательным или 0" :
                isNaN(height) ? "Параметр высоты является отрицательным или 0" :
                    isNaN(weight) ? "Параметр веса является отрицательным или 0" :
                        "ERROR! UNEXPECTED VALUE";
        var badRes = document.getElementById("js-res");
        badRes.innerHTML = `
                    <h2>${isNanValue}</h2>
    `;

        document.getElementById("calc").scrollIntoView();
    } else {

        let param = Math.floor((width * long * height) / 1000 * (1 + weight / 10));

        const input1 = parseInt(document.querySelector('select[id = "typeName0"]').value);
        const input2 = parseInt(document.querySelector('select[id = "typeName1"]').value);
        const input3 = parseInt(document.querySelector('select[id = "typeName2"]').value);
        const input4 = parseInt(document.querySelector('select[id = "typeName3"]').value);
        const input5 = parseInt(document.querySelector('select[id = "typeName4"]').value);
        const input6 = parseInt(document.querySelector('select[id = "typeName5"]').value);


        if (param < 150) {
            param = Math.floor(100 + param / 10);
        }

        let res = +param + +input1 + +input2 + +input3 + +input4 + +input5 + +input6;

        var myRes = document.getElementById("js-res");
        myRes.innerHTML = `
        <img src="./assets/img/calc/H96b1eab34ff64826a813d7df07809ada4_cut-photo.ru.jpg" alt="">
                    <h2>Итоговая стоимость доставки составляет:</h2>

                    <div class="calc_form-result-wrap">
                        <label for=""><p>Размер и вес посылки</p><p>${param} &#8381;</p></label>
                        
                        <label for=""><p>Тип содержимого</p><p>${document.querySelector('select[id = "typeName0"]').value} &#8381;</p></label>
                        <label for=""><p>Место доставки</p><p>${document.querySelector('select[id = "typeName1"]').value} &#8381;</p></label>
                        <label for=""><p>Тип доставки</p><p>${document.querySelector('select[id = "typeName2"]').value} &#8381;</p></label>
                        <label for=""><p>Скорость доставки</p><p>${document.querySelector('select[id = "typeName3"]').value} &#8381;</p></label>
                        <label for=""><p>Дополнительная упаковка</p><p>${document.querySelector('select[id = "typeName4"]').value} &#8381;</p></label>
                        <label for=""><p>Страховка</p><p>${document.querySelector('select[id = "typeName5"]').value} &#8381;</p></label>
                    </div>

                    <div class="calc_form-result-price">
                        <label for=""><p>Итого</p><p>${res} &#8381;</p></label>
                    </div>
    `;

        document.getElementById("calc").scrollIntoView();
    }
}

function regexForInput(value) {
    const pattern = /^\d+$/; // Regex pattern to match only digits
    if (value.match(pattern) && parseInt(value) > 0) {
        return parseInt(value);
    } else {
        return NaN;
    }
}

