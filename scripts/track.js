
    // APIIII
// c369d2ac-0999-4696-9ac4-1cff2ae4a71a
// c6e4d651-bb40-4f20-805d-29e9e8c4d334
// вывод результата запроса

const form = document.querySelector('form');

const calculateProgram = async (event) => {
    event.preventDefault();

    let trackInputValue = document.getElementById("trackInput").value;

    const {trackNumber, xCoordinate, yCoordinate,  active} = await
        fetch('http://140.82.58.91:3333/api/v1/tracker/findByNumber/' + trackInputValue, {
            method: 'GET',
            headers: {
                "Content-Type": "text/plain"
            }
        })
        .then(response => response.json())
        .then(trackers => {
        return {
            id: trackers.id,
            dateCreated: trackers.dateCreated,
            lastUpdate: trackers.lastUpdate,
            trackNumber: trackers.trackNumber,
            xCoordinate: trackers.xcoordinate,
            yCoordinate: trackers.ycoordinate,
            active: trackers.active
        };
        
        });

    console.log(trackNumber, xCoordinate, yCoordinate,  active);

    let trackLocation = " Ваша посылка в пути";

    if (active) {
        trackLocation = "Ваша посылка в отделении"
    } else {        
        trackLocation = "Ваша посылка в пути"
    }


    document.querySelector('.track_wrap__output-num').innerHTML = `Результаты поиска по трекеру: <br> <span> ${trackNumber} </span>`;
    document.querySelector('.track_wrap__output-location-wrap').innerHTML =`    
        <div class="track_wrap__output-location">
            <img src="./assets/img/track/truck-delivery-svgrepo-com.svg" alt="">
             <p>${trackLocation}</p>
        </div>
    `;

    //  yamap

    document.getElementById("map").innerHTML = "";
    
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                    center: [xCoordinate,yCoordinate],
                    zoom: 16
                }, {
                searchControlProvider: 'yandex#search'
            }),
    
            // Создаём макет содержимого.
            MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            );
            
            var trackerCoord = [xCoordinate, yCoordinate, false];
    
            myPlacemarkWithContent = new ymaps.Placemark([trackerCoord[0],trackerCoord[1]], {
                hintContent: 'Активен: ' + (trackerCoord[2] === true ? 'да' : 'нет')
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Размеры метки.
                iconImageSize: [48, 48],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-24, -24],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
            
                myMap.geoObjects.add(myPlacemarkWithContent);
            
            
    });
    
}

form.addEventListener('submit', calculateProgram);


